# include shared zsh options
source ~/local/config/includes/zsh/options.zsh

# include shared alias
source ~/local/config/includes/zsh/alias.zsh

# configuration for archlinux

alias ls="ls --color"

# include shared execute
source ~/local/config/includes/zsh/execute.zsh

# for golang
export GOPATH=~/local/golang
