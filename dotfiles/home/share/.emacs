
;; -- System settings --

;; detect envrioentments

(defvar run-linux
  (equal system-type 'gnu/linux))
(defvar run-darwine
  (equal system-type 'darwine))

(defvar run-cli
  (equal window-system nil))
(defvar run-cocoa
  (equal window-system 'ns))

;; stop inisialize message
(setq inhibit-startup-message t)

;; load path
(setq load-path
    (append
        (list   nil
                "~/local/config/config/emacs.d/elisp")
        load-path))

;; elisp manage
(require 'auto-install)
(setq auto-install-directory "~/config/config/emacs.d/elisp")
(auto-install-compatibility-setup)

;; (require 'install-elisp)
;; (setq install-elisp-repository-directory "~/config/config/emacs.d/elisp")

;; -- Edit settings --

;; enable *scratch* buffer logging
(require 'auto-save-buffers)
(run-with-idle-timer 0.5 t 'auto-save-buffers "" "\\.txt$")
(defvar my-scratch-filename
  (expand-file-name "~/.emacs.d/.scratch"))
(defun my-init-scratch ()
  (set-buffer (get-buffer "*scratch*"))
  (when (file-exists-p my-scratch-filename)
    (erase-buffer)
    (insert-file-contents my-scratch-filename))
  (setq buffer-file-name my-scratch-filename))

;; mode definition for file extensions
(setq auto-mode-alist
      (append '(( "\.atom" . xml-mode))
              auto-mode-alist))

;; tab char
(setq-default tab-width 4 indent-tabs-mode nil)

;; no backup
(setq make-backup-files nil)
(setq auto-save-default nil)

;; input method
;; (require 'ibus)
;; (add-hook 'after-init-hook 'ibus-mode-on)
(set-language-environment 'utf-8)
(prefer-coding-system 'utf-8)
(set-terminal-coding-system 'utf-8)
(set-keyboard-coding-system 'utf-8)
(set-buffer-file-coding-system 'utf-8)
(set-default-coding-systems 'utf-8)
;; (ibus-define-common-key ?\C-\s nil)
;; (setq ibus-cursor-color '("red" "blue" "limegreen"))
;; (global-set-key [zenkaku-hankaku] 'ibus-toggle)

;; --  Display settings --

;; buffer move
(setq windmove-wrap-around t)
(windmove-default-keybindings)

;; font settings on mac
;; copied form http://d.hatena.ne.jp/setoryohei/20110117/1295336454
(if run-cocoa
    (let* ((size 16)
           (asciifont "Ricty") ; ASCII fonts
           (jpfont "Ricty") ; Japanese fonts
           (h (* size 10))
           (fontspec (font-spec :family asciifont))
           (jp-fontspec (font-spec :family jpfont)))
      (set-face-attribute 'default nil :family asciifont :height h)
      ;; (set-face-bold-p 'bold nil)
      (set-fontset-font t 'japanese-jisx0213.2004-1 jp-fontspec)
      (set-fontset-font t 'japanese-jisx0213-2 jp-fontspec)
      (set-fontset-font t 'japanese-jisx0213-1 jp-fontspec)
      (set-fontset-font t 'japanese-jisx0212 jp-fontspec)
      (set-fontset-font t 'japanese-jisx0208 jp-fontspec)
      (set-fontset-font t 'katakana-jisx0201 jp-fontspec)
      (set-fontset-font t '(#x0080 . #x024F) fontspec) 
      (set-fontset-font t '(#x0370 . #x03FF) fontspec)))

(if run-cocoa
    (dolist (elt '(("^-apple-hiragino.*" . 1.2)
                   (".*osaka-bold.*" . 1.2)
                   (".*osaka-medium.*" . 1.2)
                   (".*courier-bold-.*-mac-roman" . 1.0)
                   (".*monaco cy-bold-.*-mac-cyrillic" . 0.9)
                   (".*monaco-bold-.*-mac-roman" . 0.9)))
      (add-to-list 'face-font-rescale-alist elt)))

;; -- Command settings --

(defun insert-w3cdtf ()
  "Insert W3CDTF"
  (interactive)
  (insert (shell-command-to-string "date +%Y-%m-%dT%H:%M:%S+09:00")))


;; -- Mode settings
(require 'coffee-mode)
(add-hook 'coffee-mode-hook
          '(lambda ()
             (set (make-local-variable 'tab-width) 4)))

(require 'lua-mode)
(autoload 'lua-mode "lua-mode" "Lua editing mode." t)
(add-to-list 'auto-mode-alist '("\\.lua$" . lua-mode))
(add-to-list 'interpreter-mode-alist '("lua" . lua-mode))

;; -- Application settings --

;; --

(my-init-scratch)
