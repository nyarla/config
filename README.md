# これは何？

*にゃるら*こと岡村　直樹(24)が使ってるdotfilesです。

# ディレクトリ構造

    $root/
        # Makefile生成
        Makefile.PL
        # dotfiles置き場
        dotfiles/
            # ルートディレクトリに配置するdotfiles
            root/
                # 複数の環境で共有
                share/
                # 各マシン固有
                {user}@{machine}/
            home/
                share/
                {user}@{machine}/
        # 関連ファイル置き場
        config/
        

# 使い方

    $ cd ~/local
    $ git clobe git@github.com:nyarla/config.git config
    $ cd config
    $ perl Makefile.PL
    $ make # or sudo make

# `Makefile.PL`に関するtips

`Makefile.PL`はCore module以外依存してないので、
Perlさえ入っていれば、どんな環境でも使用できます。

また、ディレクトリ構造は上記構造をサポートしていて、
`{user}@{machine}`に環境依存dotfilesを、
`share`に複数の環境共有のdotfilesを配置することができます。

ちなみに同名のファイルが`{user}@{machine}`と`share`にあった場合、
`{user}@{machine}`の方が優先されます。

# ライセンス

特にライセンスつけてるわけではありませんが、
これ良いなーと思ったらコードをパクっても問題ありません。

というかパクられるために公開してる訳で、
パクったというか参考になったよーとかあれば、うれしい限りです。

# 管理者

* 岡村　直樹 (にゃるら) *nyarla[ at ]thotep.net*
* [Twitter](https://twitter.com/nyarla)
* [Facebook](https://www.facebook.com/nyarlax)