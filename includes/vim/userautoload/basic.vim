" Text
set textwidth=0

" Files
set nobackup
set autoread
set noswapfile

" Buffer
set hidden

" Edit
set backspace=indent,eol,start
set formatoptions=lmoq
set whichwrap=b,s,<,>,[,],~

" Views
set ruler
set showmode
set mouse=a
