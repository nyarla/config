set nocompatible
filetype off

if has('vim_starting')
    set runtimepath+=~/.vim/bundle/neobundle.vim/
endif

call neobundle#rc(expand('~/.vim/bundle/'))

NeoBundleFetch 'Shougo/neobundle.vim'

NeoBundle 'vim-coffee-script'
NeoBundle 'elixir-lang/vim-elixir'
NeoBundle 'ekalinin/Dockerfile.vim'
NeoBundle 'https://go.googlecode.com/hg-history/go1.2/misc/vim/', { 'name': 'golang', 'type': 'hg' }

NeoBundle 'editorconfig/editorconfig-vim'

NeoBundle 'muzzl.vim'

filetype plugin indent on
NeoBundleCheck
