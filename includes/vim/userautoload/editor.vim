" Editor
set number
set showmatch

" Indent (Tab)
set autoindent
set smartindent
set tabstop=4
set softtabstop=4
set shiftwidth=4
set expandtab
set smarttab

" Search
set incsearch

" Completion
set wildmenu
set wildmode=list:longest,full

