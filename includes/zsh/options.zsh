# zsh configuration
autoload -Uz colors
colors

# prompt
PROMPT="%(!.#.$) "
RPROMPT='%F{green}<%f%U%n%u%F{yellow}@%f%m%F{green}>%f %F{green}[%f%~%F{green}]%f'

