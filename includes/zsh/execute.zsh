if [ -x "$(which keychain)" ]; then
    keychain ~/.ssh/id_rsa
    source ~/.keychain/`uname -n`-sh
fi;

if [ -x "$(which rbenv)" ]; then
    eval "$(rbenv init -)"
fi;

