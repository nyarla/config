#!/usr/bin/env zsh

# for golang
export GOPATH=~/local/golang

if [ -d  $GOPATH/bin ]; then
    export PATH=$GOPATH/bin:$PATH
fi;

# for travis ci
if [ -e ~/.travis/travis.sh ]; then
    source ~/.travis/travis.sh
fi;

