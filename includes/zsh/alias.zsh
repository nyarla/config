# plackup
alias dirup="plackup -MPlack::App::Directory -e 'Plack::App::Directory->new->to_app'"

# local/bin
export PATH=~/local/bin:$PATH

# for OSX
if [ "$(uname -s)" = 'Darwin' ]; then
    if [ -x "/Applications/MacVim.app/Contents/MacOS/Vim" ]; then
        alias vim="/Applications/MacVim.app/Contents/MacOS/Vim"
    fi;
fi;

