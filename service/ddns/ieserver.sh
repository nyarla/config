#!/bin/sh

set -e

bindir=`cd $(dirname $0) && pwd`

IP_FILE="${bindir}/current.IP"
IP_LOGS="${bindir}/IP.logs"

DDNS_USER=$1
DDNS_DOMAIN=$2
DDNS_PASS=$3

CHECK_URL='http://ieserver.net/ipcheck.shtml'
UPDATE_URL="https://ieserver.net/cgi-bin/dip.cgi?username=${DDNS_USER}&domain=${DDNS_DOMAIN}&=password=${DDNS_PASS}&updatehost=1"

CURRENT_IP='0.0.0.0'
NEW_IP='0.0.0.0'

if [ -e $IP_FILE ]; then
    CURRENT_IP=`cat ${IP_FILE}`
fi;

NEW_IP=`curl ${CHECK_URL}`

if [ $CURRENT_IP != $NEW_IP ]; then
    STATUS=`curl $UPDATE_URL`

    if [ $STATUS == $NEW_IP ]; then
        echo "${NEW_IP}" >$IP_FILE
        echo "status:success	from:${CURRENT_IP}	to:${NEW_IP}" >>$IP_LOGS
    else
        echo "status:abort	from:${CURRENT_IP}	to:${NEW_IP}" >>$IP_LOGS
    fi;
fi;

