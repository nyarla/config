#!/bin/sh

BASEDIR=/home/nyarla/service/frontend
LOGDIR=$BASEDIR/logs

NGINX=/home/nyarla/local/nginx/nginxes/frontend/sbin/nginx
PID=$BASEDIR/nginx.pid

WEBSITES='godoword'

DATE=`/bin/date +%Y-%m-%dT%H-%M-%S`

cd $LOGDIR;

if [ -e $PID ]; then
    for target in $WEBSITES; do
	mv access.${target}.log access.${target}.${DATE}.log
    done

    mv error.log error.${DATE}.log

    kill -USR1 `cat $PID`
fi;
