#!/bin/sh

ngxbrew install 1.2.4 frontend \
    --enable="http_gzip_static,http_realip,http_ssl,http_stub_status" \
    --configure="--user=nyarla --group=users"

